/*
 * factorial.c - updating the header comment
 */

#include <stdio.h>
#include <limits.h>
#include <math.h>

/* this returns the highest long we can use */
long maxlong(void)
{
  return LONG_MAX;
}

double upper_bound(long n)
{
  if (n >= 0 && n < 6)
    {
      return 121; /* 5! + 1 - technically by the exercise we should check that 121 is less than LONG_MAX, but I can't think of a situation where that wouldn't be true */
    }
  
  return pow((float)n/2, n);
}

int main(void) 
{
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }

    /* return 0 as everything is ok */
    return 0;
}
